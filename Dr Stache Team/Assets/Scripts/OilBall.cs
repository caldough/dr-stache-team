using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OilBall : MonoBehaviour
{
   public float force = 40;
    public GameObject oilBall;

    public GameObject ground;

    public GameObject spill;
    public Transform spillSpawner;
    public Transform groundRotation;



    

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody>().AddForce(transform.forward * force, ForceMode.Impulse);

        //GetComponent<EnemyMovement>().max;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {  
            Destroy(oilBall);
            Instantiate(spill, spillSpawner.position,groundRotation.rotation);
        }
    }
}