using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float min = 2f;
    public float max = 9f;

    bool isHit = false;

    //public float speed = 2f;

    //public float currentPos;

    //public Vector3 positionToMoveTo;

    
    
    // Use this for initialization
    void Start()
    {
        //StartCoroutine(LerpPosition(positionToMoveTo, 3));
        min = transform.position.x;
        max = transform.position.x + 6;

        isHit = false;
    }
    void Awake()
    {
        //float t = Time.time;
    }
    /*IEnumerator LerpPosition(Vector3 targetPosition, float duration)
    {
        float time = 0;
        Vector3 startPosition = transform.position;
        while (time < duration)
        {
            transform.position = Vector3.Lerp(startPosition, targetPosition, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
        transform.position = targetPosition;
    }*/

    // Update is called once per frame
    void Update()
    {
        if (isHit == false)
        {
            transform.position = new Vector3(Mathf.PingPong(Time.time * 2, max - min) + min, transform.position.y, transform.position.z);
        }
        else if (isHit == true)
        {
            transform.position = new Vector3(Mathf.PingPong(Time.time * 0, max - min) + min, transform.position.y, transform.position.z);
        }
        

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Board")
        {
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.tag == "BasketBall")
        {
            StartCoroutine(BallHit());

            Destroy(collision.gameObject);

        }
    }

    IEnumerator BallHit()
    {
        isHit = true;
        //transform.position = new Vector3(Mathf.PingPong(Time.time * 0, max - min) + min, transform.position.y, transform.position.z);
        //min = transform.position;
        yield return new WaitForSeconds(5);
        //max = transform.position.x + 6;
        isHit = false;
        print("hello");
    }

}
