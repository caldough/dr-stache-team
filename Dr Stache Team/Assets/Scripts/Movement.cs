using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class Movement : MonoBehaviour
{
    public CharacterController controller;


    public float speed = 1f;

    public Transform boardspawner;
    public GameObject board;

    public Transform ballspawner;
    public GameObject basketBall;

    public Transform oilspawner;
    public GameObject oilBall;

    public Transform cameraLook;


    //If the player can use the gadget again
    public bool doAgain;

    //for which gadget is being used
    private float GadCount = 0;

    //gravity
    Vector3 velocity;
    public float gravity = -20f;

    //ground check
    public Transform groundCheck;
    public float groundDistance = .4f;
    public LayerMask groundMask;
    bool isGrounded;

    //jumping
    public float jumpHeight = 3f;

    

    void Start()
    {
        doAgain = true;
        
    }

    void Update()
    {
        float inSky = transform.position.y;

        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if(isGrounded && velocity.y < 0 && inSky <= 10)
        {
            velocity.y = -12f;
            gravity = -20f;
            
        }
        else if (inSky >= 10)
        {
            if (cameraLook.transform.rotation.x < .40) 
            {
                velocity.y = 0f;
                gravity = 0f;
            }
            else if (cameraLook.transform.rotation.x > .40)
            {
                velocity.y = -200f;
                gravity = -500f;
            }
            
        }
        //print(cameraLook.transform.rotation.x);

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.fixedDeltaTime);

        //jumping
        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
            
        }

        //which gadget is selected
        if (Input.GetKeyDown("1"))
        {
            GadCount = 1;
        }
        else if (Input.GetKeyDown("2"))
        {
            GadCount = 2;
        }
        else if (Input.GetKeyDown("3"))
        {
            GadCount = 3;
        }

        //gravity
        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);

        //What happens depending on selected gadget
        if (Input.GetButtonDown("Fire1"))
        {
            if (GadCount == 1 && doAgain == true)
            {
                GameObject rocketBoard = Instantiate(board, boardspawner.position, boardspawner.rotation);
                StartCoroutine(Again());
            }
            else if (GadCount == 2 && doAgain == true)
            {
                GameObject ball = Instantiate(basketBall, ballspawner.position, ballspawner.rotation);
                StartCoroutine(Again());
            }
            else if (GadCount == 3 && doAgain == true)
            {
                GameObject oil = Instantiate(oilBall, oilspawner.position, oilspawner.rotation);
                StartCoroutine(Again());
            }
            
        }


    }

     private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Puddle")
        {
            speed = 3f;
        }
        
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Puddle")
        {
            speed = 1f;
        }

    }

    IEnumerator Again()
    {
        doAgain = false;
        yield return new WaitForSeconds(1);
        doAgain = true;
    }

    
}
