using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardMovement : MonoBehaviour
{

    public float force = 10;
    public GameObject rocketboard;

    public GameObject enemy;


    // Start is called before the first frame update
    void Start()
    {
        //GetComponent<Rigidbody>().AddForce(transform.forward * force, ForceMode.Impulse);

        StartCoroutine(Launch());
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(rocketboard, 12);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Destroy(collision.gameObject);
        }
    }

    IEnumerator Launch()
    {
        yield return new WaitForSeconds(1);

        GetComponent<Rigidbody>().AddForce(transform.forward * force, ForceMode.Impulse);
    }
}
