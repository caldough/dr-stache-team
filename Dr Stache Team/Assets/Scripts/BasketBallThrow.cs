using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasketBallThrow : MonoBehaviour
{

    public float force = 40;
    public GameObject basketBall;

    public GameObject enemy;

    

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody>().AddForce(transform.forward * force, ForceMode.Impulse);

        //GetComponent<EnemyMovement>().max;
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(basketBall, 10);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            //enemy.max = transform.position.x;
        }
    }
}
